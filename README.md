This will be the repository for RateMyLab teams. As a part of our Software Engineering Project all teams have to combine efforts to finish our ultimate task of building out this product. We have 3 teams working on different aspects, one will be handling the Student UI, another the Professor UI and a team to build out the database. All teams at some point or another will need to integrate different facets of their projects.

I created this GitLab Project repository so that we can have a centralized location for those said files. I (Josh Taylor) am on the Database Team. For instance we'll start pushing out PHP files that will connect the database to web forms. Other teams can start pushing what there UI will look like and then we can establish the best course moving forward to integrate all 3 phases.

Beyond that I don't have too much information to post here but any question I can answer I'd be happy to do but honestly I'd just ask the professor.

My email: jtaylor194@student.gsu.edu
